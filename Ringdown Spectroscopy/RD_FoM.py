import numpy as np
from scipy.interpolate import interp1d
import scipy
from astropy.cosmology import Planck15 as cosmo


import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.ticker as ticker
from matplotlib import ticker, cm
from matplotlib import rc


plt.rc('font', family='serif')
plt.rc('font', family='serif')
plt.rc('xtick', labelsize='large')
plt.rc('ytick', labelsize='large')
plt.rc('text', usetex=False)
mpl.rcParams['axes.linewidth'] = 1

rc('text', usetex=True)
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]

fig_width = 12
fig_height = 9
fig_size =  [fig_width,fig_height]

params = {'axes.labelsize': 30,
          'font.family': 'serif',
          'font.size': 20,
          'legend.fontsize': 20,
          'xtick.labelsize': 20,
          'ytick.labelsize': 20,
          'axes.grid' : False,
          'text.usetex': True,
          'savefig.dpi' : 100,
          'lines.markersize' : 15, 
          'axes.formatter.limits' : (-3,3),
          'figure.figsize': fig_size}

mpl.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}'] #for \text command

mpl.rcParams['axes.linewidth'] = 1.
mpl.rcParams.update(params)
mpl.rcParams['xtick.major.size'] = 5
mpl.rcParams['ytick.major.size'] = 5
mpl.rcParams['xtick.minor.size'] = 5
mpl.rcParams['ytick.minor.size'] = 5

mpl.rcParams['xtick.direction'] = 'in'
mpl.rcParams['ytick.direction'] = 'in'
plt.rcParams['xtick.bottom'] = plt.rcParams['xtick.top']  = True
plt.rcParams['ytick.left'] = plt.rcParams['ytick.right']  = True
plt.rcParams['xtick.labeltop'] = plt.rcParams['ytick.labelright'] = False


z_dummy_list=np.sort(np.concatenate([10**np.linspace(-50,0,50),np.linspace(0,10,1000),10**np.linspace(1+1e-6,4,100)]))
z_to_distance=cosmo.luminosity_distance(z_dummy_list).to('Gpc').value
z_at_distance = interp1d(z_to_distance, z_dummy_list)




# Ringdown Energy Fits
def E22(q): return (q**2*(0.3029252255219883 + (0.5711578949502125*q)/(1 + q)**2)**2)/(1 + q)**4
def E33(q): return (q**2*(0. + ((-1 + q)*(0.15650482439876434 + (0.6706008462258303*q)/(1 + q)**2))/(1 + q))**2)/(1 + q)**4
def E21(q): return (q**2*(0. + ((-1 + q)*(0.09905206718247558 + (0.05963116856391042*q)/(1 + q)**2))/(1 + q))**2)/(1 + q)**4
def E44(q): return  (q**2*(0.12197215533929699 - (0.9639340670952932*q**2)/(1 + q)**4 - (0.18808568415521595*q)/(1 + q)**2)**2)/(1 + q)**4



# Ringdown Frequency Fits
def qnm22(a): return np.array([1.5251 - 1.1568*(1 - a)**0.1292,(1.5251 - 1.1568*(1 - a)**0.1292)/(1.4 + 2.8374/(1 - a)**0.499)])
def qnm33(a): return np.array([1.8956 - 1.3043*(1 - a)**0.1818,(1.8956 - 1.3043*(1 - a)**0.1818)/(1.8 + 4.686/(1 - a)**0.481)])
def qnm21(a): return np.array([0.6 - 0.2339*(1 - a)**0.4175,(0.6 - 0.2339*(1 - a)**0.4175)/(-0.6 + 4.7122/(1 - a)**0.2277)])
def qnm44(a): return np.array([2.3 - 1.5056*(1 - a)**0.2244,(2.3 - 1.5056*(1 - a)**0.2244)/(2.3858 + 6.2382/(1 - a)**0.4825)])



# Fits for remnant mass and spin as a function of mass ratio
def remnant_mass(q): return 1. - (0.5435561663282535*q**2)/(1. + q)**4 - (0.057190958417936644*q)/(1. + q)**2
def remnant_spin(q): return (q*(3.4641016151377544 + (2.5763*q**2)/(1. + q)**4 - (3.51712*q)/(1. + q)**2))/(1. + q)**2


MS=4.92549095e-6
Gpc=1.0292712503000002e17


def PSD_Analytic(f):
 
    Pacc = (3e-15)**2 *(1.0+(0.4e-3/f)**2)* (1.0+(f/8e-3)**4)
    Poms = (15e-12)**2 * (1.0 + (2e-3/f)**4)
    
    c=299792458.0
    L=2.5*1e9 #m: LISA arm length 
    
    f_star= 2.*np.pi*L*f/c
    R=3.0/10.0*(1/(1+0.6*(f_star)**2))
   
    Sn= 1/(L**2) * (Poms+2.0*(1.0 + np.cos(f_star)**2)*Pacc/((2*np.pi*f)**4))/R
    
    return Sn


def snr(M,z,q, Sh_detector, mode='22', redshifted_mass=False):

    '''Signal-to-noise ratio
    
    Arguments
    ---------
    M: Remnant-Mass [Msun]
    z: Redshift
    q: Mass-Ratio
    Sh_detector: PSD
    mode: harmonic (Options-> '22', '33', '21', '44')
    redshifted_mass: True [M is the redshifted mass], False [M is the source-frame mass]

    Returns
    -------
    signal-to-nois ratio.
    '''
      
    if not hasattr(M, "__len__"): M=[M]
    if not hasattr(q, "__len__"): q=[q]
    if not hasattr(z, "__len__"): z=[z]
        
    M=np.array(M)
    q=np.array(q)
    z=np.array(z)

    if redshifted_mass:
        Mz=M*MS
    else:
        Mz=(1+z)*M*MS
        
    rM, a = remnant_mass(q), remnant_spin(q);

    if mode == '22':
        epsilon = E22(q)/rM
        wr, wi =qnm22(a)
        maxW= 0.6305
    elif mode == '33':
        epsilon = E33(q)/rM
        wr, wi =qnm33(a)
        maxW= 0.5
    elif mode == '21':
        epsilon = E21(q)/rM
        wr, wi =qnm21(a)
        maxW= 0.6305
    elif mode == '44':
        epsilon = E44(q)/rM
        wr, wi =qnm44(a)
        maxW= 0.56
    else:
        print('Mode not available')
        return

    f, tau, Q = wr/(2*np.pi*Mz), Mz/wi, wr/(2*wi)
    Q=wr/(2*wi)
    d_L=cosmo.luminosity_distance(z).to('Gpc').value*Gpc
    A=4.*np.sqrt((epsilon*Q)/(f*Mz*(1 + 4*Q**2)))

    return np.sqrt((A**2*Mz**2*maxW**2*tau)/(d_L**2*Sh_detector(f)))/np.sqrt(2)
