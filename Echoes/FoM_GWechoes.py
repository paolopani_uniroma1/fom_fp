# ## Probing near-horizon structures: GW echoes
# ### by Elisa Maggio, Costantino Pacilio
# ### This file is part of the LISA FoM toolbox.

import numpy as np
import sympy as sp
import matplotlib.pyplot as plt
import cmath
import sys
from copy import deepcopy
from sympy import Piecewise
from scipy.special import gamma, zeta, digamma, polygamma
from scipy.interpolate import interp1d, CubicSpline
from scipy.integrate import quad, simps


msun = 4.925492*10**(-6)


def replace_polygamma(expr):
    for item in sp.preorder_traversal(expr):
        name = str(item).split(' ')
        if name[0] == 'polygamma(0,':
            name = str(item).split(',')[-1]
            expr = expr.subs(item,sp.Symbol('digamma('+name))
    return expr


class ECHO():
    """ 
    Analytical template for the ringdown of an exotic compact object (ECO)
    Parameters
    ----------
    f: frequency
    M: mass of the ECO
    chi: spin of the ECO
    R: absolute value of the reflectivity of the ECO
    phi_R: phase of the reflectivity of the ECO
    delta: compactness of the ECO
    t_0: starting time of the ringdown
    A_p: amplitude of the + polarization
    phi_p: phase of the + polarization
    A_c: amplitude of the x polarization
    phi_c: phase of the x polarization
    """
    
    def __init__(self,M,chi,t_0,R,phi_R,delta,A_p,phi_p,A_c=0.,phi_c=0.):
        self.params = {}
        self.keys = ['M', 'chi', 'R', 'phi_R', 'delta', 'A_p', 'phi_p', 't_0', 'phi_c', 'A_c']
        self.M = M
        self.chi = chi
        self.t_0 = t_0
        self.R = R
        self.phi_R = phi_R
        self.delta = delta
        self.A_p = A_p
        self.A_c = A_c
        self.phi_p = phi_p
        self.phi_c = phi_c
        flmn = 1/(2*np.pi)*1/M*(1-63/100*(1-chi)**(3/10));
        omegaRBH = 2*np.pi*flmn;
        ## threshold frequency
        self.fth = (omegaRBH+((-(M*(1-np.sqrt(1-chi**2)))+M*(1+np.sqrt(1-chi**2)))*np.log(3*np.sqrt(1111111111)))/(2*np.pi*((M**2)*(chi**2)+(M**2)*(1+np.sqrt(1-chi**2))**2)))/(2*np.pi);
        self.eval = False
        
    def __call__(self, f):
        if not self.eval: 
            params = {k:self.__dict__[k] for k in self.keys}
            
            ## low-frequency
            self.lowf_eval = self.lowf_().subs(params)
            self.lowf_eval = sp.lambdify('f',self.lowf_eval,modules='scipy')            

            ## high-frequency
            self.highf_eval = self.highf_().subs(params)
            self.highf_eval = sp.lambdify('f',self.highf_eval,modules='scipy')

            self.eval = True
        out = np.piecewise(np.array(f,dtype=np.cfloat),[f<=self.fth,f>self.fth],[lambda f: self.lowf_eval(f),lambda f: self.highf_eval(f)])
        return out
    
    def evaluate_Nabla(self):
        self.Nabla = {}
        for parameter in self.keys:
            self.Nabla[parameter] = self.diff_(parameter)
        return None
    
    def diff_(self,parameter):
        if parameter == 'M':
            factor = msun
        else:
            factor = 1.
        keys = deepcopy(self.keys)
        keys.remove(parameter)
        params = {k:self.__dict__[k] for k in keys}
        
        ## low-frequency
        lowf_diff = self.lowf_().subs(params)
        lowf_diff = sp.diff(lowf_diff,parameter)
        lowf_diff = lowf_diff.subs({parameter:self.__dict__[parameter]})
        lowf_diff = replace_polygamma(lowf_diff)
        lowf_diff = sp.lambdify('f',lowf_diff,modules='scipy')         

        ## high-frequency
        highf_diff = self.highf_().subs(params)
        highf_diff = sp.diff(highf_diff,parameter)
        highf_diff = highf_diff.subs({parameter:self.__dict__[parameter]})
        highf_diff = sp.lambdify('f',highf_diff,modules='scipy') 

        out = lambda f: np.piecewise(np.array(f,dtype=np.cfloat),[f<=self.fth,f>self.fth],[lambda f :lowf_diff(f),lambda f: highf_diff(f)])
        return out
    
    def lowf_(self):
        f, M, chi, absR, phiR, delta, Ap, phip, t0, phic, Ac = sp.symbols('f, M, chi, R, phi_R, delta, A_p, phi_p, t_0, phi_c, A_c')
        l,m = 2,2
        a = M*chi
        omega = 2*np.pi*f
        rp = M + sp.sqrt(M**2-a**2)
        rm = M - sp.sqrt(M**2-a**2)
        k = omega - (m*a)/(2*M*rp)

        ratio = rm/rp
        xi = 1 + sp.sqrt(1-chi**2)
        zita = complex(0,1)*(2*M*omega - m*sp.sqrt(ratio))*(ratio+1)*xi/(ratio-1)
        flmn = 1/(2*np.pi)*1/M*(1-63/100*(1-chi)**(3/10))
        Qlmn = 2*(1-chi)**(-45/100)
        omegaRBH = 2*np.pi*flmn
        omegaIBH = -flmn*np.pi/Qlmn
        omegaBH = omegaRBH + complex(0,1)*omegaIBH
        omegaBHc = omegaRBH - complex(0,1)*omegaIBH
        RBHlf = (-8*M*k*np.e**(zita*(ratio-1)/(ratio+1))*(2*M*k-complex(0,1)*(ratio-1))/((ratio-1)**2)*(-(ratio-1)*xi)**(zita*(ratio-1))*(16*(k*M)**2/(ratio-1)**2 +1)*sp.gamma(-2+zita)*sp.gamma(-1-zita)*(1800*complex(0,1)*sp.gamma(-2-zita)+(omega*M*(ratio-1)*xi)**5 * sp.gamma(3-zita)))/(sp.gamma(-2-zita)*sp.gamma(3-zita)*(1800*complex(0,1)*sp.gamma(-2+zita)+sp.gamma(3+zita)*(M*omega*(ratio-1)*xi)**5))
        kH = 1/2*(rp-rm)/(rp**2 + a**2)
        RBH = RBHlf*(np.e**(-2*np.pi*omegaRBH/kH)+1)/(np.e**(2*np.pi*(omega-omegaRBH)/kH)+1)
        
        alpha1p = complex(0,1)*np.e**(-complex(0,1)*(phip + t0*omegaBH))
        alpha1c = complex(0,1)*np.e**(-complex(0,1)*(phic + t0*omegaBH))
        alpha2p = complex(0,1)*np.e**(complex(0,1)*(phip + t0*omegaBHc))
        alpha2c = complex(0,1)*np.e**(complex(0,1)*(phic + t0*omegaBHc))
        ZBHp = np.e**(complex(0,1)*omega*t0)/(2*np.sqrt(2*np.pi))*((alpha1p*Ap+alpha1c*Ac)/(omega-omegaBH)+(alpha2p*Ap-alpha2c*Ac)/(omega+omegaBHc))
        
        R2 = absR*np.e**(complex(0,1)*phiR)
        epsilon = sp.sqrt(1-chi**2)*(M*delta)**2/(4*(rp**2))
        r0 = rp*(1+epsilon)
        x0 = 1/(sp.sqrt(M**2-a**2))*(sp.sqrt(M**2-a**2)*r0 + M*rp*sp.log((r0-rp)/M)- M*rm*sp.log((r0-rm)/M))
        KZBHm = (RBH*R2*np.e**(-2*complex(0,1)*k*x0))/(1-RBH*R2*np.e**(-2*complex(0,1)*k*x0))*ZBHp
        
        Zp = ZBHp + KZBHm
        h = 1/np.sqrt(4*np.pi)*Zp
        return h
        
    def highf_(self):
        f, M, chi, absR, phiR, delta, Ap, phip, t0, phic, Ac = sp.symbols('f, M, chi, R, phi_R, delta, A_p, phi_p, t_0, phi_c, A_c')
        l,m = 2,2
        omega = 2*np.pi*f
        flmn = 1/(2*np.pi)*1/M*(1-63/100*(1-chi)**(3/10))
        Qlmn = 2*(1-chi)**(-45/100)
        omegaRBH = 2*np.pi*flmn
        omegaIBH = -flmn*np.pi/Qlmn
        omegaBH = omegaRBH + complex(0,1)*omegaIBH
        omegaBHc = omegaRBH - complex(0,1)*omegaIBH
        alpha1p = complex(0,1)*np.e**(-complex(0,1)*(phip + t0*omegaBH))
        alpha1c = complex(0,1)*np.e**(-complex(0,1)*(phic + t0*omegaBH))
        alpha2p = complex(0,1)*np.e**(complex(0,1)*(phip + t0*omegaBHc))
        alpha2c = complex(0,1)*np.e**(complex(0,1)*(phic + t0*omegaBHc))
        
        ZBHp = np.e**(complex(0,1)*omega*t0)/(2*np.sqrt(2*np.pi))*((alpha1p*Ap+alpha1c*Ac)/(omega-omegaBH)+(alpha2p*Ap-alpha2c*Ac)/(omega+omegaBHc))
        
        Zp = ZBHp
        h = 1/np.sqrt(4*np.pi)*Zp
        return h


class Fisher():
    def __init__(self, signal, integration_method=simps, keys=None):
        self.signal = signal
        self.integration_method = simps
        if keys:
            self.keys = keys
        else:
            self.keys = self.signal.keys
        self.dim = len(self.keys)
        
    def SNR(self, fmin, fmax, nbins=1e5):
        x = np.linspace(fmin, fmax, int(nbins))
        y = 4*np.abs(self.signal(x))**2/self.psd(x)
        out = self.integration_method(y,x)
        return np.sqrt(out)
    
    def load_psd(self,psd_name):
        s = np.genfromtxt(psd_name).T
        self.psd = interp1d(s[0],s[1])
        return None
    
    def FisherMatrix(self, fmin, fmax, nbins=1e5):
        self.snr = self.SNR(fmin, fmax, nbins=nbins)
        self.fm = np.zeros((self.dim,self.dim))
        derivatives = list(self.signal.Nabla.values())
        f = np.linspace(fmin, fmax, int(nbins))
        for i in range(self.dim):
            for j in range(i,self.dim):
                y = 4*np.real(derivatives[i](f)*np.conj(derivatives[j](f)))/self.psd(f)
                self.fm[i,j] = self.integration_method(y,f)
                self.fm[j,i] = self.fm[i,j]
        self.fm /= self.snr**2
        return None
    
    def CovarianceMatrix(self):
        ifm = np.matrix(self.fm).I
        self.cov = np.zeros_like(ifm)
        for i in range(self.dim):
            for j in range(i,self.dim):
                self.cov[i,j] = ifm[i,j]/np.sqrt(ifm[i,i]*ifm[j,j])
                self.cov[j,i] = self.cov[i,j]
            self.cov[i,i] = np.sqrt(ifm[i,i])
        self.sigma = {self.keys[i]:self.cov[i,i] for i in range(self.dim)} 
        return None
    
    def ReflectivityError(self, SNRringdown):
        sigma_rel_percent = self.sigma['R']/self.signal.__dict__['R']*100/SNRringdown
        return sigma_rel_percent


## Parameters of the template for GW echoes
mass=1e6
spin=0.7
absR2=0.1
params = {'M':mass*msun, 'R':np.sqrt(absR2), 'phi_R':0, 'delta':1e-7, 'chi':spin, 'A_p':1, 'phi_p':0, 't_0':0, 'A_c':0, 'phi_c':0}

## Set fmin, fmax and SNRringdown
fmin=1e-4
fmax=1
SNRringdown=476

print('Probing near-horizon structures: GW echoes')
print('We use the low-frequency cutoff of', fmin, 'Hz and analyze a reference remnant with M = {:.0e} MSun, \u03C7={:.1f} and SNR_ringdown={:.0f}.'.format(mass,spin,SNRringdown))

echo = ECHO(**params)
echo.evaluate_Nabla()
keys = ['M', 'chi', 'R', 'phi_R', 'delta', 'A_p', 'phi_p', 't_0']
fisher = Fisher(echo, keys=keys)
fisher.load_psd('S_h_scird.txt') ##load the noise
fisher.FisherMatrix(fmin, fmax) 
fisher.CovarianceMatrix()
out = fisher.ReflectivityError(SNRringdown)

print('The measurement accuracy on the object effective reflectivity from echo searches, assuming |R|^2 = {:.1f}, is {:.1f}% (green).'.format(absR2,out))

