(* ::Package:: *)

(\[Gamma] + 
  (Rt*(E^(((2*I)*(2*f*Pi - (m*\[Chi])/(2*M*(1 + Sqrt[1 - \[Chi]^2])))*
         (M*Sqrt[M^2 - M^2*\[Chi]^2]*(1 + Sqrt[1 - \[Chi]^2])*
           (1 + (\[Delta]^2*Sqrt[1 - \[Chi]^2])/(4*(1 + Sqrt[1 - \[Chi]^2])^
               2)) + M*(M + Sqrt[M^2 - M^2*\[Chi]^2])*
           Log[(-M - Sqrt[M^2 - M^2*\[Chi]^2] + M*(1 + Sqrt[1 - \[Chi]^2])*(
                1 + (\[Delta]^2*Sqrt[1 - \[Chi]^2])/(4*(1 + Sqrt[
                     1 - \[Chi]^2])^2)))/(LL*M)] + 
          M*(-M + Sqrt[M^2 - M^2*\[Chi]^2])*
           Log[(-M + Sqrt[M^2 - M^2*\[Chi]^2] + M*(1 + Sqrt[1 - \[Chi]^2])*(
                1 + (\[Delta]^2*Sqrt[1 - \[Chi]^2])/(4*(1 + Sqrt[
                     1 - \[Chi]^2])^2)))/(LL*M)]))/Sqrt[M^2 - M^2*\[Chi]^2])*
      eta + Piecewise[
      {{(-4*E^(((1 + Sqrt[1 - \[Chi]^2])*(-4*f*M*Pi + m*Sqrt[
                (1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2])])*
             (I + Pi - (I*(1 - Sqrt[1 - \[Chi]^2]))/(1 + Sqrt[
                 1 - \[Chi]^2]) + (Pi*(1 - Sqrt[1 - \[Chi]^2]))/(1 + 
                Sqrt[1 - \[Chi]^2])))/(-1 + (1 - Sqrt[1 - \[Chi]^2])/
              (1 + Sqrt[1 - \[Chi]^2])))*
          (1 + E^((-4*Pi*(M^2*\[Chi]^2 + M^2*(1 + Sqrt[1 - \[Chi]^2])^2)*
              \[Omega]cutoff)/(-(M*(1 - Sqrt[1 - \[Chi]^2])) + 
              M*(1 + Sqrt[1 - \[Chi]^2]))))*(LL*M)^
           ((I*(-4*f*M*Pi + m*Sqrt[(1 - Sqrt[1 - \[Chi]^2])/(1 + 
                  Sqrt[1 - \[Chi]^2])])*(M*(1 - Sqrt[1 - \[Chi]^2]) + 
              M*(1 + Sqrt[1 - \[Chi]^2])))/M)*
          (-(M*(1 + Sqrt[1 - \[Chi]^2])*(-1 + (1 - Sqrt[1 - \[Chi]^2])/(1 + 
                Sqrt[1 - \[Chi]^2]))))^((I*(1 + Sqrt[1 - \[Chi]^2])*
             (4*f*M*Pi - m*Sqrt[(1 - Sqrt[1 - \[Chi]^2])/(1 + 
                  Sqrt[1 - \[Chi]^2])])*(-2 + (1 - Sqrt[1 - \[Chi]^2])/(1 + 
                Sqrt[1 - \[Chi]^2]))*(1 + (1 - Sqrt[1 - \[Chi]^2])/(1 + 
                Sqrt[1 - \[Chi]^2])))/(-1 + (1 - Sqrt[1 - \[Chi]^2])/
              (1 + Sqrt[1 - \[Chi]^2])))*((M*(1 - Sqrt[1 - \[Chi]^2]) - 
             M*(1 + Sqrt[1 - \[Chi]^2]))^(-1))^((I*(1 + Sqrt[1 - \[Chi]^2])*
             (-4*f*M*Pi + m*Sqrt[(1 - Sqrt[1 - \[Chi]^2])/(1 + 
                  Sqrt[1 - \[Chi]^2])])*(1 + (1 - Sqrt[1 - \[Chi]^2])/(1 + 
                Sqrt[1 - \[Chi]^2])))/(-1 + (1 - Sqrt[1 - \[Chi]^2])/
              (1 + Sqrt[1 - \[Chi]^2])))*
          (-(m*Sqrt[(1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2])]) + 
           2*f*M*Pi*(1 + Sqrt[1 - \[Chi]^2])*(1 + (1 - Sqrt[1 - \[Chi]^2])/
              (1 + Sqrt[1 - \[Chi]^2])))*
          (I - m*Sqrt[(1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2])] + 
           2*f*M*Pi*(1 + Sqrt[1 - \[Chi]^2]) + ((1 - Sqrt[1 - \[Chi]^2])*
             (-I + 2*f*M*Pi*(1 + Sqrt[1 - \[Chi]^2])))/
            (1 + Sqrt[1 - \[Chi]^2]))*(I - 2*m*Sqrt[(1 - Sqrt[1 - \[Chi]^2])/
              (1 + Sqrt[1 - \[Chi]^2])] + 4*f*M*Pi*(1 + Sqrt[1 - \[Chi]^2]) + 
           ((1 - Sqrt[1 - \[Chi]^2])*(-I + 4*f*M*Pi*(1 + Sqrt[
                 1 - \[Chi]^2])))/(1 + Sqrt[1 - \[Chi]^2]))*
          (-I - 2*m*Sqrt[(1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2])] + 
           4*f*M*Pi*(1 + Sqrt[1 - \[Chi]^2]) + ((1 - Sqrt[1 - \[Chi]^2])*
             (I + 4*f*M*Pi*(1 + Sqrt[1 - \[Chi]^2])))/
            (1 + Sqrt[1 - \[Chi]^2]))*Gamma[-1 + (I*(1 + Sqrt[1 - \[Chi]^2])*
              (-4*f*M*Pi + m*Sqrt[(1 - Sqrt[1 - \[Chi]^2])/(1 + 
                   Sqrt[1 - \[Chi]^2])])*(1 + (1 - Sqrt[1 - \[Chi]^2])/
                (1 + Sqrt[1 - \[Chi]^2])))/(-1 + (1 - Sqrt[1 - \[Chi]^2])/(
                1 + Sqrt[1 - \[Chi]^2]))]*
          Gamma[((-I)*(-3*I - 4*f*M*Pi*(1 + Sqrt[1 - \[Chi]^2]) + 
              m*Sqrt[(1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2])]*(1 + 
                Sqrt[1 - \[Chi]^2]) + m*((1 - Sqrt[1 - \[Chi]^2])/
                 (1 + Sqrt[1 - \[Chi]^2]))^(3/2)*(1 + Sqrt[1 - \[Chi]^2]) + 
              ((1 - Sqrt[1 - \[Chi]^2])*(3*I - 4*f*M*Pi*(1 + Sqrt[
                    1 - \[Chi]^2])))/(1 + Sqrt[1 - \[Chi]^2])))/
            (-1 + (1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2]))]*
          Gamma[((-I)*(2*I - 4*f*M*Pi*(1 + Sqrt[1 - \[Chi]^2]) + 
              m*Sqrt[(1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2])]*(1 + 
                Sqrt[1 - \[Chi]^2]) + m*((1 - Sqrt[1 - \[Chi]^2])/
                 (1 + Sqrt[1 - \[Chi]^2]))^(3/2)*(1 + Sqrt[1 - \[Chi]^2]) - 
              (2*(1 - Sqrt[1 - \[Chi]^2])*(I + 2*f*M*Pi*(1 + Sqrt[
                    1 - \[Chi]^2])))/(1 + Sqrt[1 - \[Chi]^2])))/
            (-1 + (1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2]))]*
          ((1800*I)*Gamma[(I*(-2*I - 4*f*M*Pi*(1 - Sqrt[1 - \[Chi]^2]) + 
                ((2*I)*(1 - Sqrt[1 - \[Chi]^2]))/(1 + Sqrt[1 - \[Chi]^2]) - 
                4*f*M*Pi*(1 + Sqrt[1 - \[Chi]^2]) + 
                m*Sqrt[(1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2])]*
                 (1 + Sqrt[1 - \[Chi]^2]) + m*((1 - Sqrt[1 - \[Chi]^2])/
                   (1 + Sqrt[1 - \[Chi]^2]))^(3/2)*(1 + Sqrt[1 - \[Chi]^2])))/
              (-1 + (1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2]))] + 
           32*f^5*M^5*Pi^5*(1 + Sqrt[1 - \[Chi]^2])^5*
            (-1 + (1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2]))^5*
            Gamma[(I*(3*I - 4*f*M*Pi*(1 + Sqrt[1 - \[Chi]^2]) + 
                m*Sqrt[(1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2])]*
                 (1 + Sqrt[1 - \[Chi]^2]) + m*((1 - Sqrt[1 - \[Chi]^2])/
                   (1 + Sqrt[1 - \[Chi]^2]))^(3/2)*(1 + Sqrt[1 - \[Chi]^2]) - 
                ((1 - Sqrt[1 - \[Chi]^2])*(3*I + 4*f*M*Pi*(1 + Sqrt[
                      1 - \[Chi]^2])))/(1 + Sqrt[1 - \[Chi]^2])))/
              (-1 + (1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2]))]))/
         ((1 + E^((4*Pi*(M^2*\[Chi]^2 + M^2*(1 + Sqrt[1 - \[Chi]^2])^2)*
              (-\[Omega]cutoff + 2*Pi*Abs[f]))/
             (-(M*(1 - Sqrt[1 - \[Chi]^2])) + M*(1 + Sqrt[1 - \[Chi]^2]))))*
          (-1 + (1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2]))^4*
          Gamma[(I*(-2*I - 4*f*M*Pi*(1 - Sqrt[1 - \[Chi]^2]) + 
              ((2*I)*(1 - Sqrt[1 - \[Chi]^2]))/(1 + Sqrt[1 - \[Chi]^2]) - 
              4*f*M*Pi*(1 + Sqrt[1 - \[Chi]^2]) + m*Sqrt[
                (1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2])]*(1 + 
                Sqrt[1 - \[Chi]^2]) + m*((1 - Sqrt[1 - \[Chi]^2])/
                 (1 + Sqrt[1 - \[Chi]^2]))^(3/2)*(1 + Sqrt[1 - \[Chi]^2])))/
            (-1 + (1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2]))]*
          Gamma[3 + (I*(1 + Sqrt[1 - \[Chi]^2])*(4*f*M*Pi - m*
                Sqrt[(1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2])])*
              (1 + (1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2])))/
             (-1 + (1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2]))]*
          (32*f^5*M^5*Pi^5*(1 + Sqrt[1 - \[Chi]^2])^5*
            (-1 + (1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2]))^5*
            Gamma[((-I)*(-3*I - 4*f*M*Pi*(1 + Sqrt[1 - \[Chi]^2]) + 
                m*Sqrt[(1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2])]*
                 (1 + Sqrt[1 - \[Chi]^2]) + m*((1 - Sqrt[1 - \[Chi]^2])/
                   (1 + Sqrt[1 - \[Chi]^2]))^(3/2)*(1 + Sqrt[1 - \[Chi]^2]) + 
                ((1 - Sqrt[1 - \[Chi]^2])*(3*I - 4*f*M*Pi*(1 + Sqrt[
                      1 - \[Chi]^2])))/(1 + Sqrt[1 - \[Chi]^2])))/
              (-1 + (1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2]))] + 
           (1800*I)*Gamma[((-I)*(2*I - 4*f*M*Pi*(1 + Sqrt[1 - \[Chi]^2]) + 
                m*Sqrt[(1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2])]*
                 (1 + Sqrt[1 - \[Chi]^2]) + m*((1 - Sqrt[1 - \[Chi]^2])/
                   (1 + Sqrt[1 - \[Chi]^2]))^(3/2)*(1 + Sqrt[1 - \[Chi]^2]) - 
                (2*(1 - Sqrt[1 - \[Chi]^2])*(I + 2*f*M*Pi*(1 + Sqrt[
                      1 - \[Chi]^2])))/(1 + Sqrt[1 - \[Chi]^2])))/
              (-1 + (1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2]))])*
          Gamma[(I*(3*I - 4*f*M*Pi*(1 + Sqrt[1 - \[Chi]^2]) + 
              m*Sqrt[(1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2])]*(1 + 
                Sqrt[1 - \[Chi]^2]) + m*((1 - Sqrt[1 - \[Chi]^2])/
                 (1 + Sqrt[1 - \[Chi]^2]))^(3/2)*(1 + Sqrt[1 - \[Chi]^2]) - 
              ((1 - Sqrt[1 - \[Chi]^2])*(3*I + 4*f*M*Pi*(1 + Sqrt[
                    1 - \[Chi]^2])))/(1 + Sqrt[1 - \[Chi]^2])))/
            (-1 + (1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2]))]), 
        2*Pi*Abs[f*M] <= M*(\[Omega]cutoff + 
           ((-(M*(1 - Sqrt[1 - \[Chi]^2])) + M*(1 + Sqrt[1 - \[Chi]^2]))*
             Log[3*Sqrt[1111111111]])/(2*Pi*(M^2*\[Chi]^2 + 
              M^2*(1 + Sqrt[1 - \[Chi]^2])^2)))}}, 0]))/
   (E^(((2*I)*(2*f*Pi - (m*\[Chi])/(2*M*(1 + Sqrt[1 - \[Chi]^2])))*
       (M*Sqrt[M^2 - M^2*\[Chi]^2]*(1 + Sqrt[1 - \[Chi]^2])*
         (1 + (\[Delta]^2*Sqrt[1 - \[Chi]^2])/(4*(1 + Sqrt[1 - \[Chi]^2])^
             2)) + M*(M + Sqrt[M^2 - M^2*\[Chi]^2])*
         Log[(-M - Sqrt[M^2 - M^2*\[Chi]^2] + M*(1 + Sqrt[1 - \[Chi]^2])*
             (1 + (\[Delta]^2*Sqrt[1 - \[Chi]^2])/(4*(1 + Sqrt[1 - \[Chi]^2])^
                 2)))/(LL*M)] + M*(-M + Sqrt[M^2 - M^2*\[Chi]^2])*
         Log[(-M + Sqrt[M^2 - M^2*\[Chi]^2] + M*(1 + Sqrt[1 - \[Chi]^2])*
             (1 + (\[Delta]^2*Sqrt[1 - \[Chi]^2])/(4*(1 + Sqrt[1 - \[Chi]^2])^
                 2)))/(LL*M)]))/Sqrt[M^2 - M^2*\[Chi]^2])*
    (1 - (Rt*Piecewise[{{(-4*E^(((1 + Sqrt[1 - \[Chi]^2])*(-4*f*M*Pi + 
                m*Sqrt[(1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2])])*(
                I + Pi - (I*(1 - Sqrt[1 - \[Chi]^2]))/(1 + Sqrt[
                   1 - \[Chi]^2]) + (Pi*(1 - Sqrt[1 - \[Chi]^2]))/
                 (1 + Sqrt[1 - \[Chi]^2])))/(-1 + (1 - Sqrt[1 - \[Chi]^2])/
                (1 + Sqrt[1 - \[Chi]^2])))*(1 + E^((-4*Pi*(M^2*\[Chi]^2 + 
                 M^2*(1 + Sqrt[1 - \[Chi]^2])^2)*\[Omega]cutoff)/(
                -(M*(1 - Sqrt[1 - \[Chi]^2])) + M*(1 + Sqrt[1 - \[Chi]^2]))))*
            (LL*M)^((I*(-4*f*M*Pi + m*Sqrt[(1 - Sqrt[1 - \[Chi]^2])/
                   (1 + Sqrt[1 - \[Chi]^2])])*(M*(1 - Sqrt[1 - \[Chi]^2]) + 
                M*(1 + Sqrt[1 - \[Chi]^2])))/M)*
            (-(M*(1 + Sqrt[1 - \[Chi]^2])*(-1 + (1 - Sqrt[1 - \[Chi]^2])/
                 (1 + Sqrt[1 - \[Chi]^2]))))^((I*(1 + Sqrt[1 - \[Chi]^2])*(
                4*f*M*Pi - m*Sqrt[(1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[
                     1 - \[Chi]^2])])*(-2 + (1 - Sqrt[1 - \[Chi]^2])/
                 (1 + Sqrt[1 - \[Chi]^2]))*(1 + (1 - Sqrt[1 - \[Chi]^2])/
                 (1 + Sqrt[1 - \[Chi]^2])))/(-1 + (1 - Sqrt[1 - \[Chi]^2])/
                (1 + Sqrt[1 - \[Chi]^2])))*((M*(1 - Sqrt[1 - \[Chi]^2]) - M*
                (1 + Sqrt[1 - \[Chi]^2]))^(-1))^((I*(1 + Sqrt[1 - \[Chi]^2])*(
                -4*f*M*Pi + m*Sqrt[(1 - Sqrt[1 - \[Chi]^2])/(1 + 
                    Sqrt[1 - \[Chi]^2])])*(1 + (1 - Sqrt[1 - \[Chi]^2])/
                 (1 + Sqrt[1 - \[Chi]^2])))/(-1 + (1 - Sqrt[1 - \[Chi]^2])/
                (1 + Sqrt[1 - \[Chi]^2])))*(-(m*Sqrt[(1 - Sqrt[1 - \[Chi]^2])/
                 (1 + Sqrt[1 - \[Chi]^2])]) + 2*f*M*Pi*(1 + Sqrt[
                1 - \[Chi]^2])*(1 + (1 - Sqrt[1 - \[Chi]^2])/
                (1 + Sqrt[1 - \[Chi]^2])))*
            (I - m*Sqrt[(1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2])] + 
             2*f*M*Pi*(1 + Sqrt[1 - \[Chi]^2]) + ((1 - Sqrt[1 - \[Chi]^2])*(
                -I + 2*f*M*Pi*(1 + Sqrt[1 - \[Chi]^2])))/(1 + Sqrt[
                1 - \[Chi]^2]))*(I - 2*m*Sqrt[(1 - Sqrt[1 - \[Chi]^2])/
                (1 + Sqrt[1 - \[Chi]^2])] + 4*f*M*Pi*(1 + Sqrt[
                1 - \[Chi]^2]) + ((1 - Sqrt[1 - \[Chi]^2])*(-I + 
                4*f*M*Pi*(1 + Sqrt[1 - \[Chi]^2])))/(1 + Sqrt[1 - \[Chi]^2]))*
            (-I - 2*m*Sqrt[(1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[
                  1 - \[Chi]^2])] + 4*f*M*Pi*(1 + Sqrt[1 - \[Chi]^2]) + 
             ((1 - Sqrt[1 - \[Chi]^2])*(I + 4*f*M*Pi*(1 + Sqrt[
                   1 - \[Chi]^2])))/(1 + Sqrt[1 - \[Chi]^2]))*
            Gamma[-1 + (I*(1 + Sqrt[1 - \[Chi]^2])*(-4*f*M*Pi + 
                 m*Sqrt[(1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2])])*
                (1 + (1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2])))/(
                -1 + (1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2]))]*
            Gamma[((-I)*(-3*I - 4*f*M*Pi*(1 + Sqrt[1 - \[Chi]^2]) + 
                m*Sqrt[(1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2])]*
                 (1 + Sqrt[1 - \[Chi]^2]) + m*((1 - Sqrt[1 - \[Chi]^2])/
                   (1 + Sqrt[1 - \[Chi]^2]))^(3/2)*(1 + Sqrt[1 - \[Chi]^2]) + 
                ((1 - Sqrt[1 - \[Chi]^2])*(3*I - 4*f*M*Pi*(1 + Sqrt[
                      1 - \[Chi]^2])))/(1 + Sqrt[1 - \[Chi]^2])))/
              (-1 + (1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2]))]*
            Gamma[((-I)*(2*I - 4*f*M*Pi*(1 + Sqrt[1 - \[Chi]^2]) + 
                m*Sqrt[(1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2])]*
                 (1 + Sqrt[1 - \[Chi]^2]) + m*((1 - Sqrt[1 - \[Chi]^2])/
                   (1 + Sqrt[1 - \[Chi]^2]))^(3/2)*(1 + Sqrt[1 - \[Chi]^2]) - 
                (2*(1 - Sqrt[1 - \[Chi]^2])*(I + 2*f*M*Pi*(1 + Sqrt[
                      1 - \[Chi]^2])))/(1 + Sqrt[1 - \[Chi]^2])))/
              (-1 + (1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2]))]*
            ((1800*I)*Gamma[(I*(-2*I - 4*f*M*Pi*(1 - Sqrt[1 - \[Chi]^2]) + 
                  ((2*I)*(1 - Sqrt[1 - \[Chi]^2]))/(1 + Sqrt[1 - \[Chi]^2]) - 
                  4*f*M*Pi*(1 + Sqrt[1 - \[Chi]^2]) + m*Sqrt[
                    (1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2])]*
                   (1 + Sqrt[1 - \[Chi]^2]) + m*((1 - Sqrt[1 - \[Chi]^2])/
                     (1 + Sqrt[1 - \[Chi]^2]))^(3/2)*(1 + Sqrt[1 - \[Chi]^
                       2])))/(-1 + (1 - Sqrt[1 - \[Chi]^2])/(1 + 
                   Sqrt[1 - \[Chi]^2]))] + 32*f^5*M^5*Pi^5*
              (1 + Sqrt[1 - \[Chi]^2])^5*(-1 + (1 - Sqrt[1 - \[Chi]^2])/
                 (1 + Sqrt[1 - \[Chi]^2]))^5*Gamma[(I*(3*I - 4*f*M*Pi*
                   (1 + Sqrt[1 - \[Chi]^2]) + m*Sqrt[(1 - Sqrt[1 - \[Chi]^2])/
                     (1 + Sqrt[1 - \[Chi]^2])]*(1 + Sqrt[1 - \[Chi]^2]) + 
                  m*((1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2]))^(3/2)*
                   (1 + Sqrt[1 - \[Chi]^2]) - ((1 - Sqrt[1 - \[Chi]^2])*
                    (3*I + 4*f*M*Pi*(1 + Sqrt[1 - \[Chi]^2])))/(1 + 
                    Sqrt[1 - \[Chi]^2])))/(-1 + (1 - Sqrt[1 - \[Chi]^2])/
                  (1 + Sqrt[1 - \[Chi]^2]))]))/
           ((1 + E^((4*Pi*(M^2*\[Chi]^2 + M^2*(1 + Sqrt[1 - \[Chi]^2])^2)*
                (-\[Omega]cutoff + 2*Pi*Abs[f]))/(-(M*(1 - Sqrt[
                    1 - \[Chi]^2])) + M*(1 + Sqrt[1 - \[Chi]^2]))))*
            (-1 + (1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2]))^4*
            Gamma[(I*(-2*I - 4*f*M*Pi*(1 - Sqrt[1 - \[Chi]^2]) + 
                ((2*I)*(1 - Sqrt[1 - \[Chi]^2]))/(1 + Sqrt[1 - \[Chi]^2]) - 
                4*f*M*Pi*(1 + Sqrt[1 - \[Chi]^2]) + 
                m*Sqrt[(1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2])]*
                 (1 + Sqrt[1 - \[Chi]^2]) + m*((1 - Sqrt[1 - \[Chi]^2])/
                   (1 + Sqrt[1 - \[Chi]^2]))^(3/2)*(1 + Sqrt[1 - \[Chi]^2])))/
              (-1 + (1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2]))]*
            Gamma[3 + (I*(1 + Sqrt[1 - \[Chi]^2])*(4*f*M*Pi - 
                 m*Sqrt[(1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2])])*
                (1 + (1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2])))/(
                -1 + (1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2]))]*
            (32*f^5*M^5*Pi^5*(1 + Sqrt[1 - \[Chi]^2])^5*
              (-1 + (1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2]))^5*
              Gamma[((-I)*(-3*I - 4*f*M*Pi*(1 + Sqrt[1 - \[Chi]^2]) + 
                  m*Sqrt[(1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2])]*
                   (1 + Sqrt[1 - \[Chi]^2]) + m*((1 - Sqrt[1 - \[Chi]^2])/
                     (1 + Sqrt[1 - \[Chi]^2]))^(3/2)*(1 + Sqrt[1 - \[Chi]^
                       2]) + ((1 - Sqrt[1 - \[Chi]^2])*(3*I - 4*f*M*Pi*
                      (1 + Sqrt[1 - \[Chi]^2])))/(1 + Sqrt[1 - \[Chi]^2])))/
                (-1 + (1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2]))] + 
             (1800*I)*Gamma[((-I)*(2*I - 4*f*M*Pi*(1 + Sqrt[1 - \[Chi]^2]) + 
                  m*Sqrt[(1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2])]*
                   (1 + Sqrt[1 - \[Chi]^2]) + m*((1 - Sqrt[1 - \[Chi]^2])/
                     (1 + Sqrt[1 - \[Chi]^2]))^(3/2)*(1 + Sqrt[1 - \[Chi]^
                       2]) - (2*(1 - Sqrt[1 - \[Chi]^2])*(I + 2*f*M*Pi*
                      (1 + Sqrt[1 - \[Chi]^2])))/(1 + Sqrt[1 - \[Chi]^2])))/
                (-1 + (1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2]))])*
            Gamma[(I*(3*I - 4*f*M*Pi*(1 + Sqrt[1 - \[Chi]^2]) + 
                m*Sqrt[(1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2])]*
                 (1 + Sqrt[1 - \[Chi]^2]) + m*((1 - Sqrt[1 - \[Chi]^2])/
                   (1 + Sqrt[1 - \[Chi]^2]))^(3/2)*(1 + Sqrt[1 - \[Chi]^2]) - 
                ((1 - Sqrt[1 - \[Chi]^2])*(3*I + 4*f*M*Pi*(1 + Sqrt[
                      1 - \[Chi]^2])))/(1 + Sqrt[1 - \[Chi]^2])))/
              (-1 + (1 - Sqrt[1 - \[Chi]^2])/(1 + Sqrt[1 - \[Chi]^2]))]), 
          2*Pi*Abs[f*M] <= M*(\[Omega]cutoff + 
             ((-(M*(1 - Sqrt[1 - \[Chi]^2])) + M*(1 + Sqrt[1 - \[Chi]^2]))*
               Log[3*Sqrt[1111111111]])/(2*Pi*(M^2*\[Chi]^2 + 
                M^2*(1 + Sqrt[1 - \[Chi]^2])^2)))}}, 0])/
      E^(((2*I)*(2*f*Pi - (m*\[Chi])/(2*M*(1 + Sqrt[1 - \[Chi]^2])))*
         (M*Sqrt[M^2 - M^2*\[Chi]^2]*(1 + Sqrt[1 - \[Chi]^2])*
           (1 + (\[Delta]^2*Sqrt[1 - \[Chi]^2])/(4*(1 + Sqrt[1 - \[Chi]^2])^
               2)) + M*(M + Sqrt[M^2 - M^2*\[Chi]^2])*
           Log[(-M - Sqrt[M^2 - M^2*\[Chi]^2] + M*(1 + Sqrt[1 - \[Chi]^2])*(
                1 + (\[Delta]^2*Sqrt[1 - \[Chi]^2])/(4*(1 + Sqrt[
                     1 - \[Chi]^2])^2)))/(LL*M)] + 
          M*(-M + Sqrt[M^2 - M^2*\[Chi]^2])*
           Log[(-M + Sqrt[M^2 - M^2*\[Chi]^2] + M*(1 + Sqrt[1 - \[Chi]^2])*(
                1 + (\[Delta]^2*Sqrt[1 - \[Chi]^2])/(4*(1 + Sqrt[
                     1 - \[Chi]^2])^2)))/(LL*M)]))/
        Sqrt[M^2 - M^2*\[Chi]^2]))))*
 ((Ac*E^((2*I)*f*Pi*t0)*(I/(E^(I*(\[Phi]c + t0*(I*\[Omega]I + \[Omega]R)))*
       (2*f*Pi - I*\[Omega]I - \[Omega]R)) + 
     (I*E^(t0*\[Omega]I)*(Cos[\[Phi]c + t0*\[Omega]R] + 
        I*Sin[\[Phi]c + t0*\[Omega]R]))/(2*f*Pi - I*\[Omega]I + \[Omega]R)))/
   (2*Sqrt[2*Pi]) + (Ap*E^((2*I)*f*Pi*t0)*
    (I/(E^(I*(\[Phi]p + t0*(I*\[Omega]I + \[Omega]R)))*
       (2*f*Pi - I*\[Omega]I - \[Omega]R)) + 
     (I*E^(t0*\[Omega]I)*(Cos[\[Phi]p + t0*\[Omega]R] + 
        I*Sin[\[Phi]p + t0*\[Omega]R]))/(2*f*Pi - I*\[Omega]I + \[Omega]R)))/
   (2*Sqrt[2*Pi]))
