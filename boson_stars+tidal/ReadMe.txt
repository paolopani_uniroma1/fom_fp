This notebook includes various different analyses:

a) Tidal Love number in a model-agnostic way
b) Tidal heating in a model-agnostic wat
c) Analysis of boson-star inspiral with a coherent waveform
