import numpy as np
import pandas as pd
from scipy.interpolate import CubicSpline, interp1d
import sympy as sp
from sympy import Rational
from scipy.integrate import simps
from scipy import optimize

## physical units
cc = 299792.458 ## Km/s (exact by decree)
msun = 1.98855e+30 #Kg
G = 6.67430e-11 ## m^3/kg/s from <https://physics.nist.gov/cuu/Constants/index.html> 
rsun = msun*G*1e-9/cc**2 ## Km
Mpc = 3.1e19 ## Km

## redshift calculations
def Ldist(z):
    OM = 0.286
    OLamda = 0.714
    H0 = 70
    integrand = lambda x: 1\
        /np.sqrt(OM*(1+x)**3+OLamda)
    dz = 1e-3
    X = np.arange(0,z+dz,dz)
    Y = integrand(X)
    out = cc/H0*(1+z)*simps(Y,X)
    return out

def redshift_from_dist(dL):
    foo = lambda x: Ldist(x) - dL
    out = optimize.fsolve(foo,1)
    return out

## Isco Schwarzschild
def IscoSchwarzschild(obj1,obj2):
    m1 = obj1.mass*rsun
    m2 = obj2.mass*rsun
    mtot = m1+m2
    out = cc/(np.pi*mtot*6**1.5)
    return out

## Isco Kerr
def IscoKerr(obj1,obj2,self_force=False):
    ## from the mathematica module by Andrea
    ## binary properties
    chi1 = obj1.spin
    chi2 = obj2.spin
    m1 = obj1.mass*rsun
    m2 = obj2.mass*rsun
    mtot = m1+m2
    eta = (m1*m2)/(m1+m2)**2
    # fit self force coefficients
    if self_force:
        x = np.array([-0.99,-0.9,-0.8,-0.7,-0.6,-0.5,-0.4,-0.3,-0.2,-0.1,\
                       0.,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,0.99])
        y1 = np.array([1.1903,1.1961,1.2043,1.2148,1.2282,1.2453,1.2670,1.2948,\
                       1.3303,1.3759,1.4349,1.5116,1.6118,1.7434,1.9167,2.1441,\
                       2.4388,2.8098,3.2524,3.7337,4.1440])
        y2 = np.array([0.1945,0.2020,0.2110,0.2205,0.2308,0.2417,0.2534,0.2657,\
                       0.2788,0.2923,0.3062,0.3199,0.3328,0.3435,0.3502,0.3499,\
                       0.3382,0.3097,0.2590,0.1837,0.0983])
        f1 = interp1d(x,y1)
        f2 = interp1d(x,y2)
    # 0th order
    Z1 = 1+(1-chi1**2)**(1/3)*((1+chi1)**(1/3)+(1-chi1)**(1/3))
    Z2 = np.sqrt(3*chi1**2+Z1**2)
    KIsco = 3+Z2-np.sign(chi1)*np.sqrt((3-Z1)*(3+Z1+2*Z2))
    out = cc/(np.pi*mtot)/(KIsco**1.5+chi1)
    if self_force:
        out *= (1+eta*f1(chi1)+eta*chi2*f2(chi1))
    return out

## symbolic variables
# variables
eta, delta, M, c = sp.symbols('eta, delta, M, c')
chi_s, chi_a, kappa_s, kappa_a, Lamda, Lamda2 =\
 sp.symbols('chi_s, chi_a, kappa_s, kappa_a, Lamda, Lamda_2')
v, f = sp.symbols('v, f')
tc, phic, DL = sp.symbols('t_c, phi_c, D_L')

## ----------- PHASE ROUTINES ----------

def phase_coef_():
    ''' v = (pi*M*f/cc)**(1/3)'''
    ## numerical factors
    pi = sp.pi
    gE = sp.EulerGamma
    ## spin contributions
    beta = Rational(113,3)*delta*chi_a + (113-76*eta)/3*chi_s
    
    sigma = - 5*chi_s**2/8*(1 + 156*eta + 80*delta*kappa_a + 80*(1-2*eta)*kappa_s)\
        - 5*chi_a**2/8*((1-160*eta) + 80*delta*kappa_a + 80*(1-2*eta)*kappa_s)\
        - 5*chi_s*chi_a/4*(delta + 80*(1-2*eta)*kappa_a + 80*delta*kappa_s)
    
    gamma = chi_a*delta*(-Rational(732985,2268) - 140*eta/9)\
            + chi_s*(-Rational(732985,2268) + 24260*eta/81 + 340*eta**2/9)
    
    xi = sp.pi*( 2270*chi_a*delta/3 + chi_s*(Rational(2270,3) - 520*eta) )\
         + chi_s**2*( -Rational(1344475,2016) + 829705*eta/504\
        + 3415*eta**2/9\
        + delta*kappa_a*(Rational(26015,28) - 1495*eta/6)\
        + kappa_s*(Rational(26015,28) - 44255*eta/21 - 240*eta**2) )\
        + chi_a**2*( -Rational(1344475,2016) + 267815*eta/252 - 240*eta**2\
        + delta*kappa_a*(Rational(26015,28) - 1495*eta/6)\
        + kappa_s*(Rational(26015,28) - 44255*eta/21 - 240*eta**2) )\
        + chi_s*chi_a*( kappa_a*(Rational(26015,14) - 88510*eta/21 - 480*eta**2)\
        + delta*(-Rational(1344475,1008) + 745*eta/18\
        + kappa_s*(Rational(26015,14) - 1495*eta/3)) )
    
   # zeta = delta*chi_a*( -Rational(25150083775,3048192) + 26804935*eta/6048\
   #     - 1985*eta**2/48 ) + chi_s*( -Rational(25150083775,3048192)\
   #     + 10566655595*eta/762048 - 1042165*eta**2/3024 + 5345*eta**3/36 )\
   #     + chi_s**3*( Rational(265,24) + 4035*eta/2 - 20*eta**2/3\
   #     + kappa_s*(Rational(3110,3) - 10250*eta/3 + 40*eta**2)\
   #     + delta*kappa_a*(Rational(3110,3)-4030*eta/3) - 440*(1-3*eta) )\
   #     + chi_a**3*( kappa_a*(Rational(3110,3) - 8470*eta/3)\
   #     + delta*(Rational(265,24) - 2070*eta\
   #     + kappa_s*(Rational(3110,3) - 750*eta) - 440*(1-eta)) )\
   #     + chi_s**2*chi_a*( kappa_a*(3110 - 28970*eta/3 + 80*eta**2)\
   #     + delta*(Rational(265,8) + 12055*eta/6\
   #     + kappa_s*(3110 - 10310*eta/3) - 1320*(1-eta)) )\
   #     + chi_s*chi_a**2*( Rational(265,8) - 6500*eta/3 + 40*eta**2\
   #     + kappa_s*(3110 - 27190*eta/3 + 40*eta**2)\
   #     + delta*kappa_a*(3110 - 8530*eta/3) - 1320*(1-3*eta) )

    zeta = delta*chi_a*( -Rational(25150083775,3048192) + 26804935*eta/6048\
        - 1985*eta**2/48 ) + chi_s*( -Rational(25150083775,3048192)\
        + 10566655595*eta/762048 - 1042165*eta**2/3024 + 5345*eta**3/36 )\
        + chi_s**3*( Rational(265,24) + 4035*eta/2 - 20*eta**2/3\
        + (Rational(3110,3) - 10250*eta/3 + 40*eta**2)\
        - 440*(1-3*eta) )\
        + chi_a**3*( delta*(Rational(265,24) - 2070*eta\
        + (Rational(3110,3) - 750*eta) - 440*(1-eta)) )\
        + chi_s**2*chi_a*( delta*(Rational(265,8) + 12055*eta/6\
        + (3110 - 10310*eta/3) - 1320*(1-eta)) )\
        + chi_s*chi_a**2*( Rational(265,8) - 6500*eta/3 + 40*eta**2\
        + (3110 - 27190*eta/3 + 40*eta**2)\
        - 1320*(1-3*eta) )

    ## output
    out = []
    out.append(1.)
    out.append(0.)
    out.append(Rational(3715,756) + 55*eta/9)
    ## leading SO
    out.append(-16*pi + beta)
    ## leading SS
    out.append(Rational(15293365,508032) + 27145*eta/504 + 3085*eta**2/72 + sigma)
    out.append(38645*pi/756 - 65*pi*eta/9 + gamma)
    out.append(Rational(11583231236531,4694215680) - 6848*gE/21 -\
        640*pi**2/3 + (2255*pi**2/12 - Rational(15737765635,3048192))*eta +\
        76055*eta**2/1728 - 127825*eta**3/1296 + xi )
    out.append( 77096675*pi/254016 + 378515*pi/1512*eta\
        -74045*pi/756*eta**2 + zeta )
    return out

def phase_pn_(ppN=-1):
    ''' v = (pi*M*f/cc)**(1/3)'''
    cfs = phase_coef_()
    ## set ppN order
    imax = len(cfs)
    if ppN != -1:
        imax = min(len(cfs), int(2*ppN) + 1)
    ## output
    out = 0.
    for i in range(imax):
        out += cfs[i]*(v**i)
    ## add log terms
    if ppN==-1 or int(2*ppN)>=5:
        out += cfs[5]*3*sp.log(v)*(v**5)
    if ppN==-1 or int(2*ppN)>=6:
        ## !!!
        out += - Rational(6848,63)*sp.log(64*v**3)*(v**6)
    ## add tidal deformability correction
    if ppN==-1 or int(2*ppN)>=10:
        out += -Rational(39,2)*Lamda*v**10
    if ppN==-1 or int(2*ppN)>=12:
        out += -Rational(3115,64)*Lamda*v**12+\
          Rational(6595,364)*delta*Lamda2*v**12
    return out

def phase_(ppN=-1):
    out = -sp.pi/4 + 2*c*tc/M*v**3 - phic + 3/(128*eta*v**5)*phase_pn_(ppN=ppN)
    out = out.subs('v', '(pi*M*f/c)**Rational(1,3)')
    return out

## ---------- AMPLITUDE ROUTINES ----------

def amp_coef_():
    ''' v = (pi*M*f/cc)**(1/3)'''
    ## numerical factors
    pi = sp.pi
    ## spin corrections
    sigma = (8*eta - Rational(81,32))*chi_a**2 + (17*eta/8 - Rational(81,32))*chi_s**2\
            - Rational(81,16)*delta*chi_s*chi_a
    gamma = (Rational(285197,16128) - 1579*eta/4032)*delta*chi_a +\
            (Rational(285197,16128) - 15317*eta/672 - 2227*eta**2/1008)*chi_s
    xi = (Rational(1614569,64512) - 1873643*eta/16128 + 2167*eta**2/42)*chi_a**2 + \
         (31*pi/12 - 7*pi*eta/3)*chi_s +\
         (Rational(1614569,64512) - 61392*eta/1344 + 57451*eta**2/4032)*chi_s**2 +\
         (31*pi/12 + (Rational(1614569,32256) - 165961*eta/2688)*chi_s)*delta*chi_a
    ## output
    out = []
    out.append(1)
    out.append(0)
    out.append(-Rational(323,224) + 451*eta/168)
    out.append((27*delta*chi_a/8 + (Rational(27,8) - 11*eta/6)*chi_s))
    out.append(-Rational(27312085,8128512) - 1975055*eta/338688 + 105271*eta**2/24192 + sigma)
    out.append(-85*pi/64 + 85*pi*eta/16 + gamma)
    out.append(-Rational(177520268561,8583708672) + (Rational(545384828789,5007163392)\
             - 205*pi**2/48)*eta - 3248849057*eta**2/178827264 + 34473079*eta**3/6386688 + xi)
    return out

def amp_pn_(ppN=-1):
    ''' v = (pi*M*f/cc)**(1/3)'''
    cfs = amp_coef_()
    ## set ppN order
    imax = len(cfs)
    if ppN != -1:
        imax = min(len(cfs), int(2*ppN) + 1)
    ## output
    out = 0.
    for i in range(imax):
        out += cfs[i]*(v**i)
    return out

def amp_(ppN=-1, norm='rms'):
    out = amp_pn_(ppN=ppN)*sp.sqrt(sp.pi*eta)*v**Rational(-7,2)
    ## amplitude of type 1 (from PhenomD?)
    if norm == 'ideal' or norm is None:
        # C = 1
        out *= sp.sqrt(5/24)
    elif norm == 'phenomD':
        out *= sp.sqrt(2/3)
    ## amplitude of type 2 (from Damour 2000)
    elif norm == 'rms':
        # C = 2/5
        # see Eq. (7.177) in Maggiore's book and discussion below
        out *= sp.sqrt(1/30)
    else:
        # ideal
        out *= norm*sp.sqrt(5/24)
    ## "distance" normalization M^2/(c DL)
    out *= M**2/(c*DL)
    out = out.subs('v', '(pi*M*f/c)**Rational(1,3)')
    return out


## ---------- WAVEFORM CLASS ----------

def evaluate(expr, **kwargs):
    variables = []
    for k in kwargs.keys():
        variables.append(k)
    nexpr = sp.lambdify(variables, expr, 'numpy')
    out = nexpr(**kwargs)
    return out

class TaylorF2():
    
    def __init__(self, obj1, obj2, DL=100, PN=-1, norm=1):
        self.PN = PN
        self.norm = norm
        self.params_ = {}
        self.params_['M'] = (obj1.mass + obj2.mass)*rsun
        self.params_['kappa_s'] = 0.5*(obj1.kappa + obj2.kappa)
        self.params_['kappa_a'] = 0.5*(obj1.kappa - obj2.kappa)
        self.params_['eta'] = obj1.mass*obj2.mass/(obj1.mass + obj2.mass)**2
        self.params_['mu'] = self.params_['eta']*self.params_['M']
        self.params_['M_c'] = self.params_['M']*self.params_['eta']**0.6
        self.params_['delta'] = (obj1.mass - obj2.mass)/(obj1.mass + obj2.mass)
        self.params_['q'] = obj1.mass / obj2.mass
        self.params_['chi_s'] = 0.5*(obj1.spin +  obj2.spin)
        self.params_['chi_a'] = 0.5*(obj1.spin - obj2.spin)
        self.params_['Lamda'] = 16/13*(self.params_['q']**4*(12+self.params_['q'])*obj1.Lamda+\
                (1+12*self.params_['q'])*obj2.Lamda)/(1+self.params_['q'])**5
        self.params_['Lamda_2'] = 1/1319*(self.params_['q']**4*\
                (-11005-7996*self.params_['q']+\
                1319*self.params_['q']**2)*obj1.Lamda+\
                (-1319+7996*self.params_['q']+\
                11005*self.params_['q']**2)*obj2.Lamda)/(1+self.params_['q'])**6
        self.params_['D_L'] = DL*Mpc
    
    def phase(self):
        out = phase_(ppN=self.PN)
        return out

    def amp(self,ppN=0):
        out =  amp_(ppN=ppN, norm=self.norm)
        return out
    
    def __call__(self, f, tc, phic):
        phase = self.phase()
        amp = self.amp(ppN=0)
        replace = {'t_c':tc, 'phi_c':phic, 'c':cc, 'f':f}
        amp = evaluate(amp, **{**self.params_,**replace})
        phase = evaluate(phase, **{**self.params_,**replace})
        return amp, phase


## ---------- FISHER ROUTINES ----------

def substitute(expr,**kwargs):
    variables = []
    for k,v in kwargs.items():
        variables.append((k,v))
    out = expr.subs(variables)
    return out

class Fisher():
    
    def __init__(self, ph, amp, Sn, fmin, fmax, method=simps):
        self.Sn = Sn
        self.ph = ph
        self.amp = amp
        self.fmin = fmin
        self.fmax = fmax
        self.variables = None
        self.method = method
    
    def integral(self, a, b):
        integrand = lambda x: 4*a(x)*b(x)/self.Sn(x)
        X = np.linspace(self.fmin,self.fmax,10**4)
        Y = integrand(X)
        integral = self.method(Y, X)
        return integral
    
    def vector(self, variables, **kwargs):
        self.variables = variables
        fv = []
        for v in variables:
            expr = sp.diff(self.ph,v)*self.amp
            expr = substitute(expr, **kwargs)
            expr = sp.lambdify('f', expr, 'numpy')
            fv.append(expr)
        return fv
    
    def SNR(self):
        A = sp.lambdify('f', self.amp, 'numpy')
        SNR = np.sqrt(self.integral(A,A))
        return SNR
    
    def Gamma(self, fv):
        n = len(fv)
        Gamma = np.zeros((n,n))
        for i in range(n):
            #Gamma[i,i] = self.integral(fv[i],fv[i])
            for j in range(i,n):
                Gamma[i,j] = self.integral(fv[i],fv[j])
                Gamma[j,i] = Gamma[i,j]
        return Gamma
    
    def Sigma(self, Gamma, priors=None):
        if priors is not None:
            Gamma += priors
        ## Sigma = np.linalg.inv(Gamma)
        Sigma = np.matrix(Gamma).I
        ## compute covariance matrix
        CM = np.zeros_like(Sigma)
        for i in range(len(Sigma)):
            CM[i,i] = 1.
            for j in range(i,len(Sigma)):
                CM[i,j] = Sigma[i,j]/np.sqrt(Sigma[i,i]*Sigma[j,j])
        ## compute uncertainties
        sigmas = {}
        for i in range(len(Sigma)):
            sigmas[self.variables[i]] = Sigma[i,i]**0.5
        return sigmas, CM


## ---------- OBJECT ROUTINES ----------
def BS_Lamda(Beta):
    cfs = np.array([-0.828,20.99,-99.1,149.7])*np.sqrt(2/np.pi)/8
    ## sennett fit
    expr_lamda = (-0.828+20.99/sp.log(Lamda)-99.1/sp.log(Lamda)**2\
          +149.7/sp.log(Lamda)**3)*sp.sqrt(2/sp.pi)/8
    ## compute lambda_min and beta_max according to sennet
    Lamda_min = np.float(sp.solve(sp.simplify(sp.diff(expr_lamda,Lamda)))[1])
    Beta_max = np.float(sp.lambdify(Lamda,expr_lamda,'numpy')(Lamda_min))
    #Beta_max = 0.06110333
    rr = Beta/0.06
    cfs[0] = cfs[0] - Beta_max*rr
    roots = np.roots(cfs[::-1])
    root = np.real(roots)[2]
    return np.exp(1/root)


class CompactObject():
    '''
    mass: in units of Msun
    spin: dimensionless
    '''
    def __init__(self, mass, spin, EOS=None, Mmax=None):
        self.mass = mass
        self.spin = spin
        if EOS is None:
            self.kappa = 1.
            self.Lamda = 0.
            self.C = 0.5
        elif EOS == 'BS':
            self.Mmax = Mmax
            self.MB = Mmax/0.06
            self.Beta = mass/Mmax*0.06
            self.Lamda = BS_Lamda(self.Beta)
            self.kappa = np.exp(0.61)*self.Lamda**0.3
            self.C = 0.5 ## this is not true, but for the moment is not important
        else:
            filename = EOS
            df = pd.read_csv(filename,sep=' ',engine='python',\
              names=['logp','mass','radius','lamda','mass2']).sort_values('mass')
            lm = interp1d(df['mass'],df['lamda'])
            rm = interp1d(df['mass'],df['radius'])
            self.Lamda = lm(mass).item()
            radius = rm(mass).item()
            self.C = mass/radius
            self.kappa = 1
